package com.gis.droneapp.gimbal;

import android.content.Context;
import android.util.AttributeSet;

import com.gis.droneapp.common.BasePushDataView;
import com.gis.droneapp.common.GISApplication;
import com.gis.droneapp.utils.GISModuleVerificationUtil;

import dji.sdk.Gimbal.DJIGimbal;

/**
 * Class for getting gimbal information.
 */
public class PushGimbalDataView extends BasePushDataView {
    public PushGimbalDataView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected String getOSDTextResourceString() {
        return "Gimbal test";
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (GISModuleVerificationUtil.isGimbalModuleAvailable()) {
            GISApplication.getProductInstance().getGimbal().setGimbalStateUpdateCallback(
                    new DJIGimbal.GimbalStateUpdateCallback() {
                        @Override
                        public void onGimbalStateUpdate(DJIGimbal djiGimbal,
                                                        DJIGimbal.DJIGimbalState djiGimbalState) {
                            mStringBuffer.delete(0, mStringBuffer.length());

                            mStringBuffer.append("PitchInDegrees: ").
                                append(djiGimbalState.getAttitudeInDegrees().pitch).append("\n");
                            mStringBuffer.append("RollInDegrees: ").
                                append(djiGimbalState.getAttitudeInDegrees().roll).append("\n");
                            mStringBuffer.append("YawInDegrees: ").
                                append(djiGimbalState.getAttitudeInDegrees().yaw).append("\n");

                            mHandler.sendEmptyMessage(CHANGE_TEXT_VIEW);
                        }
                    }
            );
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (GISModuleVerificationUtil.isGimbalModuleAvailable()) {
            GISApplication.getProductInstance().getGimbal().setGimbalStateUpdateCallback(null);
        }
    }
}

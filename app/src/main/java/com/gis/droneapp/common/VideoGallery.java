package com.gis.droneapp.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.gis.droneapp.R;

/**
 * Created by rajaj on 6/20/2016.
 */
public class VideoGallery extends Activity {

    private Cursor videocursor;
    private int video_column_index;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_gallery);
        try {
            GridView gridview = (GridView) findViewById(R.id.gridview);
            String[] projection = {MediaStore.Video.Media._ID};
            String selection=MediaStore.Video.Media.DATA +" like?";
            String[] selectionArgs=new String[]{"%Dji_Sdk_Test%"};
            videocursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    projection, selection, selectionArgs, MediaStore.Video.Media.DATE_TAKEN + " DESC");
            count = videocursor.getCount();

            if(count==0)
            {
                VideoGallery.this.finish();
                return;
            }

            gridview.setAdapter(new VideoAdapter(this));
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        int video_column_index = videocursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media._ID);
                        videocursor.moveToPosition(position);
                        String filename= videocursor.getString(video_column_index);
                        Uri uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,filename);
                        Intent i = new Intent(VideoGallery.this, VideoActivity.class);
                        i.putExtra("videoPath", getPath(uri));
                        VideoGallery.this.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public class VideoAdapter extends BaseAdapter {
        private Context vContext;
        private LayoutInflater inflater = null;

        public VideoAdapter(Context c) {
            vContext = c;
            inflater = (LayoutInflater) vContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return count;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            private ImageView video_img;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int id = 0;
            try {
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = inflater.inflate(R.layout.videos_item, null);
                    holder.video_img = (ImageView) convertView.findViewById(R.id.video_item_image);

                    video_column_index = videocursor
                            .getColumnIndexOrThrow(MediaStore.Video.Media._ID);
                    videocursor.moveToPosition(position);
                    id = videocursor.getInt(video_column_index);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.video_img.setImageBitmap(getImage(id));
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("MainActivity:getView()-135: ex " + ex.getClass() + ", " + ex.getMessage());
            }

            return convertView;
        }

        // Create the thumbnail on the fly
        private Bitmap getImage(int id) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            Bitmap thumb = MediaStore.Video.Thumbnails.getThumbnail(
                    vContext.getContentResolver(),
                    id, MediaStore.Video.Thumbnails.MICRO_KIND, options);
            return thumb;
        }
    }
}

package com.gis.droneapp.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.gis.droneapp.R;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

/**
 * Created by Venkat on 10/04/16.
 */
public class SplashActivity extends Activity {


    ShimmerTextView tv;
    Shimmer shimmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        tv = (ShimmerTextView) findViewById(R.id.splash_text);


        shimmer = new Shimmer();
        shimmer.start(tv);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                shimmer.cancel();
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                SplashActivity.this.finish();
            }
        }, 4000);

    }
}

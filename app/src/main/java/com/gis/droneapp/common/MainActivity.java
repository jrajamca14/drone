package com.gis.droneapp.common;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gis.droneapp.R;
import com.gis.droneapp.djidemo.SwitchboardActivity;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import java.util.Stack;

import de.greenrobot.event.EventBus;
import dji.sdk.SDKManager.DJISDKManager;
import dji.sdk.base.DJIBaseProduct;

public class MainActivity extends AppCompatActivity implements DJIBaseProduct.DJIVersionCallback {

    public static final String TAG = MainActivity.class.getName();

    private FrameLayout mContentFrameLayout;

    private ObjectAnimator mPushInAnimator;
    private ObjectAnimator mPushOutAnimator;
    private ObjectAnimator mPopInAnimator;
    private LayoutTransition mPopOutTransition;

    private Stack<SetViewWrapper> mStack;

    private TextView mTitleTextView,version_txt;
    private Button btn_tabfly,btn_active_track;

    ShimmerTextView tv;
    Shimmer shimmer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * each time the USB from the RC is connected/disconnected,
         * the phone will prompt the user to select the app they want
         * to connect
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }
        setContentView(R.layout.activity_main);
        setupActionBar();
        mContentFrameLayout = (FrameLayout) findViewById(R.id.framelayout_content);
        tv = (ShimmerTextView) findViewById(R.id.splash_text);
        shimmer = new Shimmer();
        shimmer.start(tv);
        initParams();
        EventBus.getDefault().register(this);
        btn_tabfly=(Button)findViewById(R.id.btn_tabfly);
        version_txt=(TextView)findViewById(R.id.version);
        btn_tabfly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SwitchboardActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        updateVersion();
        DJIBaseProduct product = DJISDKManager.getInstance().getDJIProduct();
        if(product != null) {
            product.setDJIVersionCallback(this);
        }
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(GISApplication.FLAG_CONNECTION_CHANGE);
//        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    String version = null;
    private void updateVersion() {

        DJIBaseProduct product = DJISDKManager.getInstance().getDJIProduct();
        if(product != null) {
            version = product.getFirmwarePackageVersion();
        }

        if(version == null) {
            version = "1.1";
        }
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                version_txt.setText("GIS SDK Version: " + version);
            }
        });

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.actionbar_custom);
            actionBar.setDisplayHomeAsUpEnabled(false);
            mTitleTextView = (TextView)(actionBar.getCustomView().findViewById(R.id.title_tv));
        }
    }

    private void setupInAnimations() {
        mPushInAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.slide_in_right);
        mPushOutAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.fade_out);
        mPopInAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.fade_in);
        ObjectAnimator popOutAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.slide_out_right);

        mPushOutAnimator.setStartDelay(100);

        mPopOutTransition = new LayoutTransition();
        mPopOutTransition.setAnimator(LayoutTransition.DISAPPEARING, popOutAnimator);
        mPopOutTransition.setDuration(popOutAnimator.getDuration());
    }

    private void initParams() {
        setupInAnimations();

        mStack = new Stack<SetViewWrapper>();
        View view = mContentFrameLayout.getChildAt(0);
        mStack.push(new SetViewWrapper(view, R.string.activity_component_list));
    }

    private void pushView(SetViewWrapper wrapper) {
        if (mStack.size() <= 0) return;

        mContentFrameLayout.setLayoutTransition(null);

        int titleId = wrapper.getTitleId();
        View showView = wrapper.getView();

        int preTitleId = mStack.peek().getTitleId();
        View preView = mStack.peek().getView();

        mStack.push(wrapper);

        mContentFrameLayout.addView(showView);

        mPushOutAnimator.setTarget(preView);
        mPushOutAnimator.start();

        mPushInAnimator.setTarget(showView);
        mPushInAnimator.setFloatValues(mContentFrameLayout.getWidth(), 0);
        mPushInAnimator.start();

        refreshTitle();
    }

    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            refreshTitle();
        }

    };

    private void refreshTitle() {
        try {
//            if(mStack.size() > 1) {
//                SetViewWrapper wrapper = mStack.peek();
//                mTitleTextView.setText(wrapper.getTitleId());
//            } else if(mStack.size() == 1) {
//                DJIBaseProduct product = GISApplication.getProductInstance();
//                if(product != null && product.getModel() != null) {
//                    mTitleTextView.setText("" + product.getModel().getDisplayName());
//                } else {
//                    mTitleTextView.setText(R.string.app_name);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void popView() {

        if (mStack.size() <= 1) {
            finish();
            return;
        }

        SetViewWrapper removeWrapper = mStack.pop();

        View showView = mStack.peek().getView();
        View removeView = removeWrapper.getView();

        int titleId = mStack.peek().getTitleId();
        int preTitleId = 0;
        if (mStack.size() > 1) {
            preTitleId = mStack.get(mStack.size() - 2).getTitleId();
        }

        mContentFrameLayout.setLayoutTransition(mPopOutTransition);
        mContentFrameLayout.removeView(removeView);

        mPopInAnimator.setTarget(showView);
        mPopInAnimator.start();

        refreshTitle();

    }

    @Override
    public void onBackPressed() {
        if (mStack.size() > 1) {
            popView();
        } else {
            super.onBackPressed();
        }
    }

    public void onEventMainThread(SetViewWrapper wrapper) {
        pushView(wrapper);
    }

    public void onEventMainThread(SetViewWrapper.Remove wrapper) {

        if (mStack.peek().getView() == wrapper.getView()) {
            popView();
        }

    }

    @Override
    public void onProductVersionChange(String s, String s2) {

    }
}

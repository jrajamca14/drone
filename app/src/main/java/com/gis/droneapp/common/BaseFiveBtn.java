package com.gis.droneapp.common;

import android.app.Service;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gis.droneapp.R;

/**
 * Created by dji on 15/12/20.
 */
public abstract class BaseFiveBtn extends RelativeLayout implements View.OnClickListener {

    protected Button mBtn1;
    protected Button mBtn2;
    protected Button mBtn3;
    protected Button selectAll;
    protected Button playback;
    protected TextView mTexInfo;

    public BaseFiveBtn(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context, attrs);
    }

    private void initUI(Context context, AttributeSet attrs) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
        View content = layoutInflater.inflate(R.layout.view_five_btn, null, false);
        addView(content, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        try {
            mBtn1 = (Button) findViewById(R.id.btn_first);
            mBtn2 = (Button) findViewById(R.id.btn_second);
            mBtn3 = (Button) findViewById(R.id.btn_third);
            selectAll= (Button) findViewById(R.id.selectAll);
            playback= (Button) findViewById(R.id.playback);
            mTexInfo = (TextView) findViewById(R.id.text_info);

            mTexInfo.setText(getString(getInfoResourceId()));


            mBtn1.setText(getString(getBtn1TextResourceId()));
            mBtn2.setText(getString(getBtn2TextResourceId()));
            mBtn3.setText(getString(getBtn3TextResourceId()));

            selectAll.setText(getString(getBtn4TextResourceId()));
            playback.setText(getString(getBtn5TextResourceId()));
            mBtn1.setOnClickListener(this);
            mBtn2.setOnClickListener(this);
            mBtn3.setOnClickListener(this);
            selectAll.setOnClickListener(this);
            playback.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getString(int id) {
        return getResources().getString(id);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_first :
                getBtn1Method();
                break;

            case R.id.btn_second :
                getBtn2Method();
                break;

            case R.id.btn_third:
                getBtn3Method();
                break;
            case R.id.selectAll:
                getBtn4Method();
                break;
            case R.id.playback:
                getBtn5Method();
                break;
        }
    }

    protected abstract int getBtn1TextResourceId();
    protected abstract int getBtn2TextResourceId();
    protected abstract int getBtn3TextResourceId();
    protected abstract int getBtn4TextResourceId();
    protected abstract int getBtn5TextResourceId();
    protected abstract int getInfoResourceId();


    protected abstract void getBtn1Method();
    protected abstract void getBtn2Method();
    protected abstract void getBtn3Method();
    protected abstract void getBtn4Method();
    protected abstract void getBtn5Method();

}

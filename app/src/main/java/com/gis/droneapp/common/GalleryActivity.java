package com.gis.droneapp.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.gis.droneapp.R;

/**
 * Created by rajaj on 6/17/2016.
 */
public class GalleryActivity extends Activity {

    private Cursor cursor;
    private int columnIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_layout);
        try {
            // Note that Gallery view is deprecated in Android 4.1---
            String[] projection = {MediaStore.Images.Media._ID};
            // Create the cursor pointing to the SDCard
            cursor = managedQuery( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    MediaStore.Images.Media.DATA + " like ? ",
                    new String[] {"%Dji_Sdk_Test%"},
                    null);
            // Get the column index of the image ID
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            cursor.moveToPosition(0);
            int imageID = cursor.getInt(columnIndex);
            galleryImage(imageID);
            Gallery gallery = (Gallery) findViewById(R.id.gallery1);
            gallery.setAdapter(new ImageAdapter(this));
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position,long id)
                {
                    cursor.moveToPosition(position);
                    int imageID = cursor.getInt(columnIndex);
                    galleryImage(imageID);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryImage(int imageID)
    {
        ImageView imageView = (ImageView) findViewById(R.id.image1);
        // obtain the image URI
        Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(imageID));
        String url = uri.toString();
        // Set the content of the image based on the image URI
        int originalImageId = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
        Bitmap b = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(),
                originalImageId, MediaStore.Images.Thumbnails.MINI_KIND, null);
        imageView.setImageBitmap(b);
    }

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private int itemBackground;
        public ImageAdapter(Context c)
        {
            context = c;
            // sets a grey background; wraps around the images
            TypedArray a =obtainStyledAttributes(R.styleable.MyGallery);
            itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
            a.recycle();
        }
        // returns the number of images
        public int getCount() {
            return cursor.getCount();
        }
        // returns the ID of an item
        public Object getItem(int position) {
            return position;
        }
        // returns the ID of an item
        public long getItemId(int position) {
            return position;
        }
        // returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(context);
            cursor.moveToPosition(position);
            // Get the current value for the requested column
            int imageID = cursor.getInt(columnIndex);
            // obtain the image URI
            Uri uri = Uri.withAppendedPath( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(imageID) );
            String url = uri.toString();
            // Set the content of the image based on the image URI
            int originalImageId = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.length()));
            Bitmap b = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(),
                    originalImageId, MediaStore.Images.Thumbnails.MINI_KIND, null);
            imageView.setLayoutParams(new Gallery.LayoutParams(250, 250));
            imageView.setBackgroundResource(itemBackground);
            imageView.setImageBitmap(b);
            return imageView;
        }
    }
}

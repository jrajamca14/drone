package com.gis.droneapp.common;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.gis.droneapp.R;

/**
 * Created by rajaj on 6/20/2016.
 */
public class VideoActivity extends Activity {


    String videopath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);

        try {
            final VideoView videoView =(VideoView)findViewById(R.id.videoView1);
            //Creating MediaController
            MediaController mediaController= new MediaController(this);
            mediaController.setAnchorView(videoView);

            //specify the location of media file
            if (getIntent().getStringExtra("videoPath") != null)
                videopath = getIntent().getStringExtra("videoPath");
            Uri uri= Uri.parse(videopath);
            //Setting MediaController and URI, then starting the videoView
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(uri);
            videoView.requestFocus();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            VideoActivity.this.finish();
        }
    }
}

package com.gis.droneapp.camera;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;

import com.gis.droneapp.R;
import com.gis.droneapp.common.BaseFiveBtn;
import com.gis.droneapp.common.GISApplication;
import com.gis.droneapp.common.Utils;
import com.gis.droneapp.utils.GISModuleVerificationUtil;

import java.io.File;

import dji.sdk.Camera.DJICamera;
import dji.sdk.Camera.DJICameraSettingsDef;
import dji.sdk.Camera.DJIPlaybackManager;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIError;

/**
 * Created by dji on 16/1/6.
 */
public class PlaybackDownloadView extends BaseFiveBtn {

    private DJICamera mCamera;
    private DJIPlaybackManager mPlaybackManager;
    public Handler messageHandler;
    Context context;
    returnvalue ret;


    public PlaybackDownloadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        Looper looper=Looper.myLooper();
        messageHandler = new MessageHandler(looper);
    }

    public void setReceiver(returnvalue receiver) {
        ret = receiver;
    }

    /**
     * Before the playback commands are sent to the aircraft, the camera work mode should be set
     * to playback mode.
     */
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (GISModuleVerificationUtil.isCameraModuleAvailable()) {
            mCamera = GISApplication.getAircraftInstance().getCamera();

            mCamera.setCameraMode(
                    DJICameraSettingsDef.CameraMode.Playback,
                    new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {

                        }
                    }
            );
            if (GISModuleVerificationUtil.isPlaybackAvailable()) {
                mPlaybackManager = mCamera.getPlayback();
                mPlaybackManager.setDJICameraPlayBackStateCallBack(
                        new DJIPlaybackManager.DJICameraPlayBackStateCallBack() {
                    @Override
                    public void onResult(DJIPlaybackManager.DJICameraPlaybackState
                                                 djiCameraPlaybackState) {
                        if (djiCameraPlaybackState.playbackMode.equals(
                                DJICameraSettingsDef.CameraPlaybackMode.SinglePhotoPlayback))
                            mPlaybackManager.enterMultiplePreviewMode();

                        if (djiCameraPlaybackState.playbackMode.equals(
                                DJICameraSettingsDef.CameraPlaybackMode.MultipleMediaFilesDisplay))
                            mPlaybackManager.enterMultipleEditMode();
                    }
                });
            } else {
                Utils.setResultToToast(getContext(), "Not support");
            }
        }
    }
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (GISModuleVerificationUtil.isCameraModuleAvailable()) {
            GISApplication.getProductInstance().getCamera().setCameraMode(
                    DJICameraSettingsDef.CameraMode.ShootPhoto,
                    new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {

                        }
                    }
            );
        }
    }
    @Override
    protected int getBtn1TextResourceId() {
        return R.string.playback_download_select_0;
    }

    @Override
    protected int getBtn2TextResourceId() {
        return R.string.playback_download_select_1;
    }

    @Override
    protected int getBtn3TextResourceId() {
        return R.string.playback_download_download;
    }

    @Override
    protected int getBtn4TextResourceId() {
        return R.string.playback_download_download4;
    }
    @Override
    protected int getInfoResourceId() {
        if(!GISModuleVerificationUtil.isPlaybackAvailable()){
            return R.string.not_support_playback;
        }else {
            return R.string.support_playback;
        }

    }
    @Override
    protected int getBtn5TextResourceId() {
        return R.string.playback_download_download5;
    }

    int count=0;

    @Override
    protected void getBtn1Method() {
        if (GISModuleVerificationUtil.isPlaybackAvailable()) {
            mPlaybackManager = GISApplication.getProductInstance().getCamera().getPlayback();
            mPlaybackManager.unselectAllFilesInPage();
        }
    }

    @Override
    protected void getBtn2Method() {
        if (GISModuleVerificationUtil.isPlaybackAvailable()) {
            mPlaybackManager = GISApplication.getProductInstance().getCamera().getPlayback();
            mPlaybackManager.toggleFileSelectionAtIndex(count++);
        }
    }
    int progresss=0;
    @Override
    protected void getBtn3Method() {
        // Download Button
        if (GISModuleVerificationUtil.isPlaybackAvailable()) {
            mPlaybackManager = GISApplication.getProductInstance().getCamera().getPlayback();

            File destDir = new File(Environment.getExternalStorageDirectory().
                    getPath() + "/Dji_Sdk_Test/");
            mPlaybackManager.downloadSelectedFiles(destDir,
                    new DJIPlaybackManager.CameraFileDownloadCallback() {

                        @Override
                        public void onStart() {
                            Message message = Message.obtain();
                            message.obj = "Progress Start";
                            messageHandler.sendMessage(message);
                        }

                        @Override
                        public void onEnd() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Message message = Message.obtain();
                            message.obj = e.toString();
                           // messageHandler.sendMessage(message);
                        }

                        @Override
                        public void onProgressUpdate(int i) {
                            progresss=i;
                            Message message = Message.obtain();
                            message.obj = "Progress: "+i;
                            messageHandler.sendMessage(message);
                        }
                    });
        }
    }

    @Override
    protected void getBtn4Method() {
        if (GISModuleVerificationUtil.isPlaybackAvailable()) {
            mPlaybackManager = GISApplication.getProductInstance().getCamera().getPlayback();
            mPlaybackManager.selectAllFilesInPage();
        }
    }

    @Override
    protected void getBtn5Method() {
        Message message = Message.obtain();
        message.obj =progresss;
        messageHandler.sendMessage(message);
    }

    class MessageHandler extends Handler {
        public MessageHandler(Looper looper) {
            super(looper);
        }
        public void handleMessage(Message msg) {
            try {
                try {
                    if(msg.obj.toString().startsWith("Progress")) {
                        mTexInfo.setText((String) msg.obj);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if((int)msg.obj==100)
                {
                    ret.returnProgress();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface returnvalue
    {
        public void returnProgress();
    }
}
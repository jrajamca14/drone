package com.gis.droneapp.djidemo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.gis.droneapp.R;
import com.gis.droneapp.common.ActiveTrack;
import com.gis.droneapp.common.BaseActivity;
import com.gis.droneapp.common.GISApplication;
import com.gis.droneapp.common.GPSTracker;
import com.gis.droneapp.common.MapActivity;
import com.gis.droneapp.common.TapFlyActivity;
import com.gis.droneapp.common.Utils;
import com.gis.droneapp.utils.GISModuleVerificationUtil;
import com.github.glomadrian.materialanimatedswitch.MaterialAnimatedSwitch;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.SimpleDateFormat;

import dji.sdk.Battery.DJIBattery;
import dji.sdk.Camera.DJICamera;
import dji.sdk.Camera.DJICameraSettingsDef;
import dji.sdk.Camera.DJIMedia;
import dji.sdk.Camera.DJIPlaybackManager;
import dji.sdk.Codec.DJICodecManager;
import dji.sdk.FlightController.DJIFlightController;
import dji.sdk.FlightController.DJIFlightControllerDataType;
import dji.sdk.FlightController.DJIFlightControllerDelegate;
import dji.sdk.MissionManager.DJIMissionManager;
import dji.sdk.MissionManager.DJIWaypointMission;
import dji.sdk.Products.DJIAircraft;
import dji.sdk.SDKManager.DJISDKManager;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.base.DJIError;

public class SwitchboardActivity extends BaseActivity implements View.OnClickListener,TextureView.SurfaceTextureListener,GoogleMap.OnMapClickListener, OnMapReadyCallback,LocationListener {

    private ImageView mCaptureBtn,play_back;
    ImageView btnLand,takeoff;ImageView tapfly,activeTrack;
    private ToggleButton mRecordBtn;
    MaterialAnimatedSwitch toggleButton1;
    private TextView recordingTime;
    protected TextureView mVideoSurface = null;

    private ImageView btnHome;

    static DJIBaseProduct mProduct = null;
    private GoogleMap gMap;
    public static boolean checkmode=false;
    private Marker droneMarker = null;
    private DJIFlightController mFlightController;
    private DJIMissionManager mMissionManager;
    private ImageButton mPushDrawerIb,mPushDrawerIb1;
    private SlidingDrawer mPushDrawerSd,mPushDrawerSd1;

    ProgressBar batteryprogress;
    TextView batteryStatus;

    private final int MSG_RESULT_TAKEOFF = 1;
    private final int MSG_RESULT_LANDING = 2;
    private final int MSG_RESULT_Home = 3;
    private final int MSG_RESULT_MOTOR_ON = 4;
    private final int MSG_RESULT_MOTOR_OFF = 5;
    private final int MSG_VIRTUAL_STICK_ENABLE = 6;
    private final int MSG_VIRTUAL_STICK_DISABLE = 7;
    private final int MSG_VIRTUAL_STICK_RUN = 8;
    private final int MSG_VIRTUAL_STICK_VERTICAL_THROTTLE = 9;
    private final int MSG_VIRTUAL_STICK_ROLL_PITCH_MODE = 10;
    private final int MSG_VIRTUAL_STICK_YAW_MODE = 11;
    private final int MSG_VIRTUAL_STICK_ORIENTATION_MODE = 12;

    final public int MSG_BATTERY = 13;
    final public int MSG_BATTERY_ERROR = 14;
    final public int MSG_BATTERY_NO_CONNECTION = 15;

    GPSTracker gps;
    double latitude; // latitude
    double longitude; // longitude

    private float altitude = 100.0f;
    private float mSpeed = 10.0f;

    private DJIWaypointMission mWaypointMission;
    protected static final String TAG = "GIS";
    private DJIMedia mMedia;

    public Handler messageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_switchboard);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                                Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                                Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                                Manifest.permission.READ_PHONE_STATE,
                        }
                        , 1);
            }
            mProduct = DJISDKManager.getInstance().getDJIProduct();
            takeoff=(ImageView)findViewById(R.id.btn_takeoff);
            btnHome=(ImageView)findViewById(R.id.btnHome);
            btnLand=(ImageView)findViewById(R.id.btnLand);
            tapfly=(ImageView)findViewById(R.id.tapfly);
            activeTrack=(ImageView)findViewById(R.id.activeTrack);

            batteryprogress=(ProgressBar)findViewById(R.id.batteryprogress);
            batteryStatus = (TextView) findViewById(R.id.batteryStatus);
            tapfly.setOnClickListener(this);
            activeTrack.setOnClickListener(this);

            btnHome.setOnClickListener(this);
            btnLand.setOnClickListener(this);

            mVideoSurface = (TextureView)findViewById(R.id.video_previewer_surface);

            recordingTime = (TextView) findViewById(R.id.timer);
            mCaptureBtn = (ImageView) findViewById(R.id.btn_capture);
            mRecordBtn = (ToggleButton) findViewById(R.id.btn_record);
            toggleButton1= (MaterialAnimatedSwitch) findViewById(R.id.toggleButton1);
            play_back = (ImageView) findViewById(R.id.play_back);

            mPushDrawerIb = (ImageButton)findViewById(R.id.pointing_drawer_control_ib);
            mPushDrawerSd = (SlidingDrawer)findViewById(R.id.pointing_drawer_sd);

            mPushDrawerIb1 = (ImageButton)findViewById(R.id.pointing_drawer_control_left);
            mPushDrawerSd1 = (SlidingDrawer)findViewById(R.id.pointing_drawer_sd_left);
            mPushDrawerIb.setOnClickListener(this);
            mPushDrawerIb1.setOnClickListener(this);
            if (null != mVideoSurface) {
                mVideoSurface.setSurfaceTextureListener(this);
            }
            switchCameraMode(DJICameraSettingsDef.CameraMode.ShootPhoto);
            mCaptureBtn.setOnClickListener(this);
            mRecordBtn.setOnClickListener(this);
            toggleButton1.setOnCheckedChangeListener(new MaterialAnimatedSwitch.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(boolean isChecked) {
                    if (isChecked) {
                        switchCameraMode(DJICameraSettingsDef.CameraMode.RecordVideo);
                        mRecordBtn.setVisibility(View.VISIBLE);
                        mCaptureBtn.setVisibility(View.GONE);
                        checkmode=true;
                    } else {
                        switchCameraMode(DJICameraSettingsDef.CameraMode.ShootPhoto);
                        mCaptureBtn.setVisibility(View.VISIBLE);
                        mRecordBtn.setVisibility(View.GONE);
                        checkmode=false;
                    }
                }
            });
//        recordingTime.setVisibility(View.INVISIBLE);

            play_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkmode) {
                        Intent intent = new Intent(SwitchboardActivity.this, PreviewActivity.class);
                        intent.putExtra("CheckMode", checkmode);
                        SwitchboardActivity.this.startActivity(intent);
                    }
                    else
                    {
                        Intent intent1 = new Intent(SwitchboardActivity.this, PreviewActivity.class);
                        intent1.putExtra("CheckMode", checkmode);
                        SwitchboardActivity.this.startActivity(intent1);
                    }
                  }
                });


            mRecordBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        startRecord();
                    } else {
                        stopRecord();
                    }
                }
            });

            takeoff.setOnClickListener(this);
            // The callback for receiving the raw H264 video data for camera live view
            mReceivedVideoDataCallBack = new DJICamera.CameraReceivedVideoDataCallback() {

                @Override
                public void onResult(byte[] videoBuffer, int size) {
                    if(mCodecManager != null){
                        // Send the raw H264 video data to codec manager for decoding
                        mCodecManager.sendDataToDecoder(videoBuffer, size);
                    }else {
    //                    Log.e(TAG, "mCodecManager is null");
                    }
                }
            };

            DJICamera camera = GISApplication.getCameraInstance();
            if (camera != null) {
                camera.setDJICameraUpdatedSystemStateCallback(new DJICamera.CameraUpdatedSystemStateCallback() {
                    @Override
                    public void onResult(DJICamera.CameraSystemState cameraSystemState) {
                        if (null != cameraSystemState) {

                            int recordTime = cameraSystemState.getCurrentVideoRecordingTimeInSeconds();
                            int minutes = (recordTime % 3600) / 60;
                            int seconds = recordTime % 60;
                            final String timeString = String.format("%02d:%02d", minutes, seconds);
                            final boolean isVideoRecording = cameraSystemState.isRecording();

                            SwitchboardActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recordingTime.setText(timeString);
                                    if (isVideoRecording){
                                        recordingTime.setVisibility(View.VISIBLE);
                                    }else
                                    {
                                        recordingTime.setVisibility(View.INVISIBLE);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            try {
                MapFragment mapFragment = (MapFragment)getFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
           // initPlayBack();
            barProgressDialog = new ProgressDialog(SwitchboardActivity.this);
            barProgressDialog.setTitle("Downloading Image ...");
            barProgressDialog.setMessage("Download in progress ...");
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_HORIZONTAL);
            // Register the broadcast receiver for receiving the device connection's changes.
            IntentFilter filter = new IntentFilter();
            filter.addAction(GISApplication.FLAG_CONNECTION_CHANGE);
            registerReceiver(mReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initPlayBack()
    {
        if (GISModuleVerificationUtil.isCameraModuleAvailable()) {
            mCamera = GISApplication.getAircraftInstance().getCamera();
            mCamera.setCameraMode(
                    DJICameraSettingsDef.CameraMode.Playback,
                    new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {

                        }
                    }
            );
            if (GISModuleVerificationUtil.isPlaybackAvailable()) {
                mPlaybackManager = mCamera.getPlayback();
                mPlaybackManager.setDJICameraPlayBackStateCallBack(
                        new DJIPlaybackManager.DJICameraPlayBackStateCallBack() {
                            @Override
                            public void onResult(DJIPlaybackManager.DJICameraPlaybackState
                                                         djiCameraPlaybackState) {
                                if (djiCameraPlaybackState.playbackMode.equals(
                                        DJICameraSettingsDef.CameraPlaybackMode.SinglePhotoPlayback))
                                    mPlaybackManager.enterMultiplePreviewMode();

                                if (djiCameraPlaybackState.playbackMode.equals(
                                        DJICameraSettingsDef.CameraPlaybackMode.MultipleMediaFilesDisplay))
                                    mPlaybackManager.enterMultipleEditMode();
                            }
                        });
            } else {
                Utils.setResultToToast(SwitchboardActivity.this, "Not support");
            }
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        mProduct = DJISDKManager.getInstance().getDJIProduct();
        initMissionManager();
        initPreviewer();
        initFlightController();
        runBatteryStatus();
    }


    @Override
    protected void onDestroy() {
        if(mCodecManager != null){
            mCodecManager.destroyCodec();
        }
        try {
            GISApplication.getProductInstance().getBattery()
                    .setBatteryStateUpdateCallback(null);
            uninitPreviewer();
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }


    private void initPreviewer() {

        try {
            if (mProduct == null || !mProduct.isConnected()) {
             //   showToast(getString(R.string.disconnected));
            } else {
                if (null != mVideoSurface) {
                    mVideoSurface.setSurfaceTextureListener(this);
                }
                if (!mProduct.getModel().equals(DJIBaseProduct.Model.UnknownAircraft)) {
                    DJICamera camera = mProduct.getCamera();
                    if (camera != null){
                        // Set the callback
                        camera.setDJICameraReceivedVideoDataCallback(mReceivedVideoDataCallBack);
                    }
                }
                else
                {
                    mProduct.getAirLink().getLBAirLink().setDJIOnReceivedVideoCallback(mOnReceivedVideoCallback);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initMissionManager() {
        try {
            if (mProduct == null || !mProduct.isConnected()) {
               // setResultToToast("Disconnected");
                mMissionManager = null;
                return;
            } else {
                mMissionManager = mProduct.getMissionManager();
            }
            mWaypointMission = new DJIWaypointMission();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setResultToToast(final String string) {
        SwitchboardActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                 Toast.makeText(SwitchboardActivity.this, string, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.pointing_drawer_control_ib) {
            if (mPushDrawerSd1.isOpened()) {
                mPushDrawerSd1.animateClose();
            }
            if (mPushDrawerSd.isOpened()) {
                mPushDrawerSd.animateClose();
            } else {
                mPushDrawerSd.animateOpen();
            }
            return;
        }

        if (v.getId() == R.id.pointing_drawer_control_left) {
            if (mPushDrawerSd.isOpened()) {
                mPushDrawerSd.animateClose();
            }
            if (mPushDrawerSd1.isOpened()) {
                mPushDrawerSd1.animateClose();
            } else {
                mPushDrawerSd1.animateOpen();
            }
            return;
        }

        switch (v.getId()) {
            case R.id.btn_capture: {
                captureAction();
                break;
            }
            case R.id.btn_takeoff:
                btnTakeOff_onClick(v);
                break;
            case R.id.btnLand:
                btnLand_onClick(v);
                break;
            case R.id.btnHome:
                btnHome_onClick(v);
                break;
            case R.id.tapfly:
                Intent intent = new Intent(SwitchboardActivity.this,TapFlyActivity.class);
                SwitchboardActivity.this.startActivity(intent);
                break;
            case R.id.activeTrack:
                Intent intent1 = new Intent(SwitchboardActivity.this,ActiveTrack.class);
                SwitchboardActivity.this.startActivity(intent1);
                break;
            default:
                break;
        }
    }

    public void btnHome_onClick(View v) {
        DJIAircraft djiAircraft = GISApplication.getAircraftInstance();
        if ((GISApplication.getProductInstance() instanceof DJIAircraft) && null != GISApplication.getProductInstance()) {
            mFlightController = djiAircraft.getFlightController();
            mFlightController.goHome(new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Message msg = new Message();
                    Bundle bundle = new Bundle();
                    if (djiError != null) {
                        bundle.putString("DjiError", djiError.getDescription());
                    } else {
                        bundle.putString("DjiError", "DjiError returned no value and the home commmand was successful");
                    }
                    msg.what = MSG_RESULT_Home;
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                }
            });
        }
    }
    public void btnTakeOff_onClick(View v) {
        DJIAircraft djiAircraft = (DJIAircraft) DJISDKManager.getInstance().getDJIProduct();
        if ((GISApplication.getProductInstance() instanceof DJIAircraft) && null != GISApplication.getProductInstance()) {
            mFlightController = djiAircraft.getFlightController();
            mFlightController.takeOff(new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Message msg = new Message();
                    Bundle bundle = new Bundle();
                    if (djiError != null) {
                        bundle.putString("DjiError", djiError.getDescription());
                    } else {
                        bundle.putString("DjiError", "DjiError returned no value and takeoff was successful");
                    }
                    msg.what = MSG_RESULT_TAKEOFF;
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                }
            });
        }
    }
    public void btnLand_onClick(View v) {
        DJIAircraft djiAircraft = (DJIAircraft) DJISDKManager.getInstance().getDJIProduct();
        if ((GISApplication.getProductInstance() instanceof DJIAircraft) && null != GISApplication.getProductInstance()) {
            mFlightController = djiAircraft.getFlightController();
            mFlightController.autoLanding(new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Message msg = new Message();
                    Bundle bundle = new Bundle();
                    if (djiError != null) {
                        bundle.putString("DjiError", djiError.getDescription());
                    } else {
                        bundle.putString("DjiError", "DjiError returned no value and the landing was successful");
                    }
                    msg.what = MSG_RESULT_LANDING;
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                }
            });
        }
    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(SwitchboardActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void switchCameraMode(DJICameraSettingsDef.CameraMode cameraMode){
        DJICamera camera = GISApplication.getCameraInstance();
        if (camera != null) {
            camera.setCameraMode(cameraMode, new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError error) {

                    if (error == null) {
                     //   showToast("Switch Camera Mode Succeeded");
                    } else {
                       // showToast(error.getDescription());
                    }
                }
            });
        }
    }
    private DJICamera mCamera;
    // Method for taking photo
    private void captureAction(){
        final DJICamera camera = GISApplication.getCameraInstance();
        if (camera != null) {
            DJICameraSettingsDef.CameraShootPhotoMode photoMode = DJICameraSettingsDef.CameraShootPhotoMode.Single; // Set the camera capture mode as Single mode
            camera.startShootPhoto(photoMode, new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError error) {
                    if (error == null) {
                        showToast("take photo: success");
                       // new async().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        showToast(error.getDescription());
                    }
                }
            }); // Execute the startShootPhoto API
        }
    }

    // Method for starting recording
    private void startRecord(){
        if (GISModuleVerificationUtil.isCameraModuleAvailable()) {
            GISApplication.getProductInstance().getCamera().startRecordVideo(
                    new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError error) {
                            //success so, start recording
                            if (error == null) {
                                showToast("Record video: success");
                            } else {
                                showToast(error.getDescription());
                            }
                        }
                    }
            );
        }
    }

    // Method for stopping recording
    private void stopRecord(){
        if (GISModuleVerificationUtil.isCameraModuleAvailable()) {
            GISApplication.getProductInstance().getCamera().stopRecordVideo(
                    new DJIBaseComponent.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if(djiError == null) {
                                showToast("Stop recording: success");
                            }else {
                             //   showToast(djiError.getDescription());
                            }
                        }
                    }
            );
        }
    }


    @Override
    public void onPause() {
        uninitPreviewer();
        super.onPause();
    }
    private void uninitPreviewer() {
        DJICamera camera = GISApplication.getCameraInstance();
        if (camera != null){
            // Reset the callback
            GISApplication.getCameraInstance().setDJICameraReceivedVideoDataCallback(null);
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (mCodecManager == null) {
            mCodecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }


    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            switch (msg.what) {
                case MSG_RESULT_MOTOR_ON:
                    String motorsOnResult = "Motor On Result: " + bundle.getString("DjiError") + "\n";
                    Toast.makeText(SwitchboardActivity.this,motorsOnResult,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_RESULT_MOTOR_OFF:
                    String motorsOffResult = "Motor Off Result: " + bundle.getString("DjiError") + "\n";
                    Toast.makeText(SwitchboardActivity.this,motorsOffResult,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_RESULT_TAKEOFF:
                    String takeOffResult = "Takeoff Result: " + bundle.getString("DjiError") + "\n";
                    Toast.makeText(SwitchboardActivity.this,takeOffResult,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_RESULT_LANDING:
                    String landingResult = "Landing Result: " + bundle.getString("DjiError") + "\n";
                    Toast.makeText(SwitchboardActivity.this,landingResult,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_ENABLE:
                    String virtualStickEnable = bundle.getString("EnableVirtualStick") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickEnable,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_DISABLE:
                    String virtualStickDisable = bundle.getString("DisableVirtualStick") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickDisable,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_RUN:
                    String virtualStickRun = bundle.getString("VirtualStickRun") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickRun,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_VERTICAL_THROTTLE:
                    String virtualStickVerticalThrottle = bundle.getString("VirtualStickVerticalThrottle") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickVerticalThrottle,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_ROLL_PITCH_MODE:
                    String virtualStickRollPitchMode = bundle.getString("VirtualStickRollPitchMode") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickRollPitchMode,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_YAW_MODE:
                    String virtualStickYawMode = bundle.getString("VirtualStickYawMode") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickYawMode,Toast.LENGTH_SHORT).show();
                    break;
                case MSG_VIRTUAL_STICK_ORIENTATION_MODE:
                    String virtualStickOrientationMode = bundle.getString("VirtualStickOrientationMode") + "\n";
                    Toast.makeText(SwitchboardActivity.this,virtualStickOrientationMode,Toast.LENGTH_SHORT).show();
                    break;
            }

            return false;
        }
    });


    @Override
    public void onMapClick(LatLng latLng) {
        Intent intent=new Intent(SwitchboardActivity.this, MapActivity.class);
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (gMap == null) {
            gMap = googleMap;
            setUpMap();
        }
        try {
            gps = new GPSTracker(SwitchboardActivity.this);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            // check if GPS enabled
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            LatLng lt = new LatLng(latitude,longitude);
    //        gMap.addMarker(new MarkerOptions().position(lt));
            gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setUpMap() {
        gMap.setOnMapClickListener(this);// add the listener for click for amap object
    }

    public static boolean checkGpsCoordinates(double latitude, double longitude) {
        return (latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180) && (latitude != 0f && longitude != 0f);
    }

    double droneLocationLat,droneLocationLng;
    private void updateDroneLocation(){

        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        //Create MarkerOptions object
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pos);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.aircraft));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (droneMarker != null) {
                    droneMarker.remove();
                }

                if (checkGpsCoordinates(droneLocationLat, droneLocationLng)) {
                    droneMarker = gMap.addMarker(markerOptions);
                }
            }
        });
    }
    private void onProductConnectionChange()
    {
        mProduct = DJISDKManager.getInstance().getDJIProduct();
        initFlightController();
    }
    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onProductConnectionChange();
        }
    };


    private void initFlightController() {
        try {
            if (mProduct != null && mProduct.isConnected()) {
                if (mProduct instanceof DJIAircraft) {
                    mFlightController = ((DJIAircraft) mProduct).getFlightController();
                }
            }

            if (mFlightController != null) {
                mFlightController.setUpdateSystemStateCallback(new DJIFlightControllerDelegate.FlightControllerUpdateSystemStateCallback() {

                    @Override
                    public void onResult(DJIFlightControllerDataType.DJIFlightControllerCurrentState state) {
                        droneLocationLat = state.getAircraftLocation().getLatitude();
                        droneLocationLng = state.getAircraftLocation().getLongitude();
                        updateDroneLocation();

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void runBatteryStatus() {
        try {
            DJIAircraft djiAircraft = (DJIAircraft) DJISDKManager.getInstance().getDJIProduct();
            if (djiAircraft != null) {
                djiAircraft.getBattery().setBatteryStateUpdateCallback(new DJIBattery.DJIBatteryStateUpdateCallback() {
                    @Override
                    public void onResult(DJIBattery.DJIBatteryState djiBatteryState) {
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putInt("BatteryRemainingPercent", djiBatteryState.getBatteryEnergyRemainingPercent());
                        bundle.putInt("BatteryTemperature", djiBatteryState.getBatteryTemperature());
                        bundle.putInt("BatteryCurrentCurrent", djiBatteryState.getCurrentCurrent());
                        bundle.putInt("BatteryCurrentEnergy", djiBatteryState.getCurrentEnergy());
                        bundle.putInt("BatteryCurrentVoltage", djiBatteryState.getCurrentVoltage());
                        bundle.putInt("BatteryFullChargeEnergy", djiBatteryState.getFullChargeEnergy());
                        bundle.putInt("BatteryLifetimeRemainingPercent", djiBatteryState.getLifetimeRemainingPercent());
                        bundle.putInt("BatteryNumberOfDischarge", djiBatteryState.getNumberOfDischarge());
                        msg.what = MSG_BATTERY;
                        msg.setData(bundle);
                        mHandler1.sendMessage(msg);
                    }
                });

            } else {
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("BatteryNoConnection", "Battery Status - no connection available: ");
                msg.what = MSG_BATTERY_NO_CONNECTION;
                mHandler1.sendMessage(msg);
            }
        } catch (Exception e) {
            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("BatteryError", "Battery Error: " + e.getMessage() );
            msg.what = MSG_BATTERY_ERROR;
            mHandler1.sendMessage(msg);
        }
    }


    private Handler mHandler1 = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case MSG_BATTERY:
                    int status=bundle.getInt("BatteryRemainingPercent");
                    batteryStatus.setText(status+"%");
                    batteryprogress.setProgress(status);
                    break;

            }
            return false;
        }
    });
    private DJIPlaybackManager mPlaybackManager;
    ProgressDialog barProgressDialog;
    private void download() {
        if (GISModuleVerificationUtil.isPlaybackAvailable()) {
            mPlaybackManager = GISApplication.getProductInstance().getCamera().getPlayback();
            mPlaybackManager.toggleFileSelectionAtIndex(0);
            File destDir = new File(Environment.getExternalStorageDirectory().
                    getPath() + "/Dji_Sdk_Test/");
            mPlaybackManager.downloadSelectedFiles(destDir,
                    new DJIPlaybackManager.CameraFileDownloadCallback() {
                        @Override
                        public void onStart() {
                            Message message = Message.obtain();
                            message.what = 0;
                            messageHandler.sendMessage(message);
                        }

                        @Override
                        public void onEnd() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Message message = Message.obtain();
                            message.obj = e.toString();
                            messageHandler.sendMessage(message);
                        }

                        @Override
                        public void onProgressUpdate(int i) {
                            Message message = Message.obtain();
                            message.what = i;
                            messageHandler.sendMessage(message);

                        }
                    });
        }
    }

    class MessageHandler extends Handler {
        public MessageHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(final Message msg) {
            try {
                SwitchboardActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int i=msg.what;
                            barProgressDialog.setProgress(i);
                            if(i==100)
                                barProgressDialog.cancel();
                            else {
                                barProgressDialog.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class async extends AsyncTask
    {
        @Override
        protected Object doInBackground(Object[] params) {
            try {
                download();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}


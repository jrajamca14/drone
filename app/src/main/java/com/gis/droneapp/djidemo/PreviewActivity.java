package com.gis.droneapp.djidemo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.ViewGroup;

import com.gis.droneapp.R;
import com.gis.droneapp.camera.PlaybackDownloadView;
import com.gis.droneapp.common.GalleryActivity;
import com.gis.droneapp.common.VideoGallery;

/**
 * Created by rajaj on 6/6/2016.
 */
public class PreviewActivity extends Activity implements PlaybackDownloadView.returnvalue {
    PlaybackDownloadView playbackCommandsView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview);
        playbackCommandsView=(PlaybackDownloadView)findViewById(R.id.playbackview);
        playbackCommandsView.setReceiver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            ViewGroup vg = (ViewGroup)(playbackCommandsView);
            vg.removeView(playbackCommandsView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void returnProgress() {
        try {

            try {
                sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,  Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(SwitchboardActivity.checkmode)
            {
                Cursor videocursor;
                int count;
                String[] projection = {MediaStore.Video.Media._ID};
                String selection=MediaStore.Video.Media.DATA +" like?";
                String[] selectionArgs=new String[]{"%Dji_Sdk_Test%"};
                videocursor = managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        projection, selection, selectionArgs, MediaStore.Video.Media.DATE_TAKEN + " DESC");
                count = videocursor.getCount();
                if(count>0) {
                    Intent intent = new Intent(PreviewActivity.this, VideoGallery.class);
                    PreviewActivity.this.startActivity(intent);
                }
            }
        else
            {
                    Intent intent = new Intent(PreviewActivity.this, GalleryActivity.class);
                    PreviewActivity.this.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

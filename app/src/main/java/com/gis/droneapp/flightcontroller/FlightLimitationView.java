package com.gis.droneapp.flightcontroller;

import android.content.Context;
import android.util.AttributeSet;

import com.gis.droneapp.R;
import com.gis.droneapp.common.BaseThreeBtnView;
import com.gis.droneapp.common.GISApplication;
import com.gis.droneapp.common.Utils;
import com.gis.droneapp.utils.GISModuleVerificationUtil;

import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIError;

/**
 * Class for flight limitation.
 */
public class FlightLimitationView extends BaseThreeBtnView {
    private boolean mRadiusLimitaionToggleFlag = true;

    public FlightLimitationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getBtn1TextResourceId() {
        return R.string.flight_limitation_set_height_limitation;
    }

    @Override
    protected int getBtn2TextResourceId() {
        return R.string.flight_limitation_toggle_enable_radius_limitation;
    }

    @Override
    protected int getBtn3TextResourceId() {
        return R.string.flight_limitation_set_radius_limitation;
    }

    @Override
    protected int getInfoResourceId() {
        return R.string.flight_limitation_description;
    }

    @Override
    protected void getBtn1Method() {
        if (GISModuleVerificationUtil.isFlightLimitationAvailable()) {
            GISApplication.getAircraftInstance().getFlightController().
                getFlightLimitation().setMaxFlightHeight(50,
                    new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Utils.setResultToToast(
                            getContext(),
                            "Result: " + (djiError == null ?
                                    "Success" : djiError.getDescription()));
                }
            });
        }
    }

    @Override
    protected void getBtn2Method() {
        if (GISModuleVerificationUtil.isFlightLimitationAvailable()) {
            GISApplication.getAircraftInstance().getFlightController()
                .getFlightLimitation().
                    setMaxFlightRadiusLimitationEnabled(mRadiusLimitaionToggleFlag,
                         new DJIBaseComponent.DJICompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {
                                Utils.showDialogBasedOnError(getContext(), djiError);
                                mRadiusLimitaionToggleFlag = mRadiusLimitaionToggleFlag ^ true;
                            }
                    });

        }
    }

    @Override
    protected void getBtn3Method() {
        if (GISModuleVerificationUtil.isFlightLimitationAvailable()) {
            GISApplication.getAircraftInstance().getFlightController().
                    getFlightLimitation().setMaxFlightRadius(40,
                        new DJIBaseComponent.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Utils.setResultToToast(
                            getContext(),
                            "Result: " + (djiError == null ?
                                    "Success" : djiError.getDescription()));
                }
            });
        }
    }
}

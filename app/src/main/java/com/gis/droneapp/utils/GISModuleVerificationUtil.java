package com.gis.droneapp.utils;

import com.gis.droneapp.common.GISApplication;

import dji.sdk.Products.DJIAircraft;
import dji.sdk.Products.DJIHandHeld;


public class GISModuleVerificationUtil {
    public static boolean isProductModuleAvailable() {
        return (null != GISApplication.getProductInstance());
    }

    public static boolean isAircraft() {
        return GISApplication.getProductInstance() instanceof DJIAircraft;
    }

    public static boolean isHandHeld() {
        return GISApplication.getProductInstance() instanceof DJIHandHeld;
    }
    public static boolean isCameraModuleAvailable() {
        return isProductModuleAvailable() &&
                (null != GISApplication.getProductInstance().getCamera());
    }

    public static boolean isPlaybackAvailable() {
        return isCameraModuleAvailable() &&
                (null != GISApplication.getProductInstance().getCamera().getPlayback());
    }

    public static boolean isMediaManagerAvailable() {
        return isCameraModuleAvailable() &&
                (null != GISApplication.getProductInstance().getCamera().getMediaManager());
    }

    public static boolean isRemoteControllerAvailable() {
        return isProductModuleAvailable() && isAircraft() &&
        (null != GISApplication.getAircraftInstance().getRemoteController());
    }

    public static boolean isFlightControllerAvailable() {
        return isProductModuleAvailable() && isAircraft() &&
        (null != GISApplication.getAircraftInstance().getFlightController());
    }

    public static boolean isCompassAvailable() {
        return isFlightControllerAvailable() && isAircraft() &&
        (null != GISApplication.getAircraftInstance().getFlightController().getCompass());
    }

    public static boolean isFlightLimitationAvailable() {
        return isFlightControllerAvailable() && isAircraft() &&
                (null != GISApplication.getAircraftInstance().
                        getFlightController().getFlightLimitation());
    }

    public static boolean isGimbalModuleAvailable() {
        return isProductModuleAvailable() &&
                (null != GISApplication.getProductInstance().getGimbal());
    }

    public static boolean isAirlinkAvailable() {
        return isProductModuleAvailable() &&
                (null != GISApplication.getProductInstance().getAirLink());
    }

    public static boolean isWiFiAirlinkAvailable() {
        return isAirlinkAvailable() &&
                (null != GISApplication.getProductInstance().getAirLink().getWiFiLink());
    }

    public static boolean isLBAirlinkAvailable() {
        return isAirlinkAvailable() &&
                (null != GISApplication.getProductInstance().getAirLink().getLBAirLink());
    }

}

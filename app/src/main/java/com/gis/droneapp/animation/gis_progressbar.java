package com.gis.droneapp.animation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.gis.droneapp.R;


public class gis_progressbar extends LinearLayout {


    ProgressBar progresbar = null;
    float centerX, centerY = 0;
    LinearLayout linear;
    View view;
    Context context;

    public gis_progressbar(Context context, AttributeSet attr) {
        super(context);
        try {
            linear = new LinearLayout(context);
            linear.setOrientation(LinearLayout.VERTICAL);
            linear.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            addView(linear);
            setContentView(R.layout.custom_progress);
            this.context = context;
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

    }

    @Override
    public void setVisibility(int visibility) {
        // TODO Auto-generated method stub
        super.setVisibility(visibility);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // TODO Auto-generated method stub
        super.onLayout(changed, l, t, r, b);
        final View childView = getChildView();
        if (childView == null) {
            return;
        }
        final int childViewWidth = childView.getMeasuredWidth();
        final int childViewHeight = childView.getMeasuredHeight();
        final int measuredHeight = getMeasuredHeight();
        childView.layout(0, measuredHeight - childViewHeight, childViewWidth,
                measuredHeight);
    }

    public void setContentView(int id) {
        view = LayoutInflater.from(getContext()).inflate(id,
                linear, false);
        progresbar = (ProgressBar) view.findViewById(R.id.progress);
        linear.addView(view);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        try {
            getChildView().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        centerX = progresbar.getWidth() / 2;
                                        centerY = progresbar.getHeight() / 2;

                                        Flip3dAnimation anim = new Flip3dAnimation(0, 360, centerX, centerY);
                                        anim.setInterpolator(new AccelerateDecelerateInterpolator());
                                        anim.setRepeatCount(Animation.INFINITE);
                                        anim.setDuration(1300);
                                        progresbar.setAnimation(anim);
                                        progresbar.startAnimation(anim);
                                    }
                                }
            );

        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    protected View getChildView() {
        final int childCount = getChildCount();
        if (childCount != 1) {
            return null;
        }
        return getChildAt(0);
    }
}
